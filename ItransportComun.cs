﻿namespace transportPublic_POO
{
    interface ItransportComun
    {
        void addStatie(string numeStrada);
        void addPasageri(string numePasager, int locOcupat, int nrBilet);
        string afisare();
    }
}
