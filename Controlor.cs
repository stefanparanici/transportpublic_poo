﻿namespace transportPublic_POO
{
    class Controlor:Persoana
    {
        public Controlor(string nume)
        {
            numeIntreg = nume;
        }
        public const double PretBilet = 2.5;
        public override string prezenta()
        {
            return $"Biletrul la control va cere domnul{numeIntreg}, Pretul biletului este {PretBilet} lei\n";
        }
    }
}
