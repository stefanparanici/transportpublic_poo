﻿

namespace transportPublic_POO
{
    class Sofer:Persoana
    {
        public Sofer(string nume)
        {
            numeIntreg = nume;
        }

        private const char CatPermis = 'D';

        public override string prezenta()
        {
            return $"La datorie {numeIntreg}, sofer de meserie, cu {CatPermis} la permis Categorie :)\n";
        }

    }
}
