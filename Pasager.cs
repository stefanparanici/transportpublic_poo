﻿

namespace transportPublic_POO
{
    class Pasager : Persoana
    {
        public Pasager(string nume,int _locOcupat,int _nrBilet)
        {
            numeIntreg = nume;
            locOcupat=_locOcupat;
            nrBilet = _nrBilet;
        }
        private int locOcupat { get; set; }
        private int nrBilet { get; set; }

        public override string prezenta()
        {
            return $"Pasagerul {numeIntreg}, cu biletul nr.{nrBilet}, a ocupat locul nr.{locOcupat}\n";
        }
    }
}