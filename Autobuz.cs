﻿using System.Collections.Generic;

namespace transportPublic_POO
{
    class Autobuz:ItransportComun
    {
        private Persoana driver;
        private Persoana controll;
        private List<Persoana> ListaPasageri = new List<Persoana>();
        private List<Statie> ListaStatii = new List<Statie>();
        private static int _id;
        public Autobuz(string numeSofer,string numeControlor)
        {
            _id = _id++;
            driver = new Sofer(numeSofer);
            controll = new Controlor(numeControlor);
        }

        public void addStatie(string numeStrada)
        {
            ListaStatii.Add(new Statie(numeStrada));
        }
        public void addPasageri(string numePasager, int locOcupat, int nrBilet)
        {
            ListaPasageri.Add(new Pasager(numePasager,locOcupat,nrBilet));
        }
        public string afisare()
        {
            string afisare = $"Autobuzul cu id:{_id}, parcurge ruta:";
            foreach (var element in ListaStatii)
            {
                afisare = afisare + element.strada + ",";
            }
            afisare = afisare + "\n";
            afisare = afisare + driver.prezenta();
            afisare = afisare + controll.prezenta();
            foreach (var element in ListaPasageri)
            {
                afisare = afisare + element.prezenta();
            }

            return afisare;
        }

    }
}
