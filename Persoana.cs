﻿

namespace transportPublic_POO
{
    abstract class Persoana
    {
        public Persoana()
        {
            id = id++;
        }
        public static int id = 0;
        protected string numeIntreg { get; set; }
        public abstract string prezenta();

    }
}
