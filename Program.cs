﻿using System;

namespace transportPublic_POO
{
    class Program
    {
        static void Main()
        {
            ItransportComun autobuz1 = new Autobuz("Ionut Popa","Daniel Carciumaru");
            ItransportComun tren1 = new Autobuz("Calinut Popa", "Vlad Boureanu");
            ItransportComun metrou1 = new Autobuz("Andrei Popescu", "Ion Garoafa");

            autobuz1.addStatie("Floararie");
            autobuz1.addStatie("Mobila");
            autobuz1.addStatie("Curcubeu");
            autobuz1.addPasageri("Ion",5,54321);
            autobuz1.addPasageri("Adrian", 8, 54451);
            Console.Write(autobuz1.afisare());
            Console.Write("\n");

            tren1.addStatie("Floararie");
            tren1.addStatie("Mobila");
            tren1.addStatie("Curcubeu");
            tren1.addPasageri("Ion", 5, 54321);
            tren1.addPasageri("Adrian", 8, 54451);
            Console.Write(autobuz1.afisare());
            Console.Write("\n");

            metrou1.addStatie("Floararie");
            metrou1.addStatie("Mobila");
            metrou1.addStatie("Curcubeu");
            metrou1.addPasageri("Ion", 5, 54321);
            metrou1.addPasageri("Adrian", 8, 54451);
            Console.Write(autobuz1.afisare());
            Console.Write("\n");
        }
    }
}
