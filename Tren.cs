﻿using System;
using System.Collections.Generic;
using System.Text;

namespace transportPublic_POO
{
    class Tren:ItransportComun
    {
        private Persoana driver;
        private Persoana controll;
        private List<Persoana> ListaPasageri = new List<Persoana>();
        private List<Statie> ListaStatii = new List<Statie>();
        private static int id = 0;
        public Tren(string numeSofer, string numeControlor)
        {
            id = id++;
            driver = new Sofer(numeSofer);
            controll = new Controlor(numeControlor);
        }

        public void addStatie(string numeStrada)
        {
            ListaStatii.Add(new Statie(numeStrada));
        }
        public void addPasageri(string numePasager, int locOcupat, int nrBilet)
        {
            ListaPasageri.Add(new Pasager(numePasager, locOcupat, nrBilet));
        }

        public string afisare()
        {
            string afisare= $"Trenul cu id:{id}, parcurge ruta:";
            foreach (var element in ListaStatii)
            {
                afisare = afisare + element.strada +",";
            }
            afisare = afisare + "\n";
            afisare = afisare + driver.prezenta();
            afisare = afisare + controll.prezenta();
            foreach (var element in ListaPasageri)
            {
                afisare = afisare + element.prezenta();
            }

            return afisare;
        }

    }
}
